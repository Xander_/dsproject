package akka.actors

import akka.actor.{Actor, ActorSelection}
import akka.actors.HSActor.{Directions, Topics}
import akka.templates.TemplateActor
import election.Election
import messages.{Message, _}

//noinspection ForwardReference
class HSActor(method: Election.Method) extends TemplateActor[Content] {
    
    import akka.templates.CustomActor._
    
    val eUID: Int = self.path.uid
    var myLeft: ActorSelection = _
    var myRight: ActorSelection = _
    
    var actualLeader: ActorSelection = _
    var leaderUID: Int = -1
    var amITheLeader = false
    var inElection = false
    
    var phaseNumber = 0
    var leftReturned = false
    var rightReturned = false
    var nParticipant = 0
    
    override def preStart: Unit = {
        super.preStart
        log.info("My Election ID is {}", eUID)
    }
    
    override protected def init(message: Template[Content]): Unit = {
        super.init(message)
        
        message match {
    
            case Message(_, _, that: DiscoverHS) =>
                log.debug("My left reference is {} and my right reference is {}", that.left, that.right)
                this.myLeft = that.left
                this.myRight = that.right
                this.nParticipant = that.cardinality
                unstashAll
    
            case _ => throw new Exception("Wrong initialization package" + message.toString)
        }
    }
    
    override def receptive: Receive = {
    
        /**
          * When a crash is detected,
          * then the first process that detects it starts the election procedure
          * by sending 2 Election Messages (/PROBING) both to the left and right reference
          *
          */
        case Message(_, _, CrashDetected(itsLeft, leader, itsRight)) =>
    
            log.info("Crash of {} detected ...", leader)
    
            if (leader == myLeft.toSerializationFormat) myLeft = itsLeft
            else if (leader == myRight.toSerializationFormat) myRight = itsRight
    
            this.inElection = true
    
            myLeft ! Message(Topics.PROBING, Directions.LEFT, ElectionHS(eUID, self, Math.pow(2, phaseNumber).toInt))
            myRight ! Message(Topics.PROBING, Directions.RIGHT, ElectionHS(eUID, self, Math.pow(2, phaseNumber).toInt))

        case message@Message(_, _, _: Victory) => OUT(message)

        case message@Message(topic, _, cnt: ElectionHS) =>
    
            HS(message); context.become(behavior = HS, discardOld = true)

        case _ =>
    }
    
    /**
      * Hirschberg and Sinclair Leader-election Algorithm
      *
      */
    private val HS: Actor.Receive = {
        
        /**
          * When an Election Message is received then
          *
          * A) If the sender UID is higher than the UID of the receiving process then
          *
          * > If the message has traversed a number of hops - 1  equals 0
          * Then the message is sent in the reverse direction (/RETURN) it has come
          * until it reaches its sender
          * > Else, it continues to be propagated through the same direction with hop = hop - 1
          *
          * B) Else If the receiving process isn't participating to the election procedure then it joins
          *
          * C) Else if the process receive its own Election Message (/PROBING) from both its own right and left references then
          *
          * > Then it has won and will send a Election Win Message
          *
          */
        case message@Message(Topics.PROBING, direction, that@ElectionHS(_, ref, hop)) =>
            
            parent forward message
            
            if (this.eUID < that.eUID) {
                
                direction match {
                    
                    case Directions.LEFT if hop - 1 > 0 =>
                        myLeft ! Message(Topics.PROBING, Directions.LEFT, ElectionHS(that.eUID, ref, hop - 1))
                    case Directions.LEFT =>
                        myRight ! Message(Topics.RETURN, Directions.RIGHT, ElectionHS(that.eUID, ref, 0))
                    
                    case Directions.RIGHT if hop - 1 > 0 =>
                        myRight ! Message(Topics.PROBING, Directions.RIGHT, ElectionHS(that.eUID, ref, hop - 1))
                    case Directions.RIGHT =>
                        myLeft ! Message(Topics.RETURN, Directions.LEFT, ElectionHS(that.eUID, ref, 0))
                    
                    case _ =>
                }
            } else if (!inElection) {
                
                log.info("Joining the election...")
                this.inElection = true
                myLeft ! Message(Topics.PROBING, Directions.LEFT, ElectionHS(eUID, self, Math.pow(2, phaseNumber).toInt))
                myRight ! Message(Topics.PROBING, Directions.RIGHT, ElectionHS(eUID, self, Math.pow(2, phaseNumber).toInt))
    
                //                parent ! Message(Topics.PROBING, Directions.LEFT, ElectionHS(eUID, self, Math.pow(2, phaseNumber).toInt))
                //                parent ! Message(Topics.PROBING, Directions.RIGHT, ElectionHS(eUID, self, Math.pow(2, phaseNumber).toInt))
                
            } else if (that.eUID == this.eUID) {
                
                direction match {
                    case Directions.LEFT => leftReturned = true
                    case Directions.RIGHT => rightReturned = true
                    case _ =>
                }
                
                if (leftReturned && rightReturned) {
                    amITheLeader = true
                    myLeft ! Message(Topics.WIN, Directions.LEFT, Victory(eUID, self))
                }
                
            }
        
        case message@Message(Topics.RETURN, direction, that: ElectionHS) =>
    
            parent forward message
    
            if (that.eUID == this.eUID) {
                
                direction match {
                    case Directions.LEFT => leftReturned = true
                    case Directions.RIGHT => rightReturned = true
                    case _ =>
                }
                
                if (leftReturned && rightReturned) {
                    
                    phaseNumber += 1
                    // In order to enhance the performance of this algorithm we can verify if the hops number
                    // is greater than N/2, that is if it has already more than half a complete round
                    myLeft ! Message(Topics.PROBING, Directions.LEFT, ElectionHS(eUID, self, Math.pow(2, phaseNumber).toInt))
                    myRight ! Message(Topics.PROBING, Directions.RIGHT, ElectionHS(eUID, self, Math.pow(2, phaseNumber).toInt))
                    
                    leftReturned = false
                    rightReturned = false
    
                    //                    parent ! Message(Topics.PROBING, Directions.LEFT, ElectionHS(eUID, self, Math.pow(2, phaseNumber).toInt))
                    //                    parent ! Message(Topics.PROBING, Directions.RIGHT, ElectionHS(eUID, self, Math.pow(2, phaseNumber).toInt))
                }
            } else direction match {
                case Directions.LEFT => myLeft ! message
                case Directions.RIGHT => myRight ! message
                case _ =>
            }

        /**
          * When a process receive an Election Win Message then
          *
          * 1) it saves the new leader
          * 2) forward such message to its left or right process, depending on the implementation
          * 3) return to phase 1 of the Election Process, waiting for another crash
          */
        case message@Message(_, _, _: Victory) =>
            OUT(message); context.become(behavior = receptive, discardOld = false)

        case _ =>
    }
    
    val OUT: Receive = {
        
        case message@Message(_, direction, Victory(leID, newLeader)) =>
    
            parent forward message
    
            this.log.info("The new leader is {} with election ID#{}", newLeader.toSerializationFormat, leID)
            this.actualLeader = newLeader
    
            if (!amITheLeader) direction match {
                case Directions.LEFT => myLeft forward message
                case _ =>
            }
    
            parent ! Message(content = End(phaseNumber))
            
            this.reset()

        case _ =>
    }
    
    private def reset(): Unit = {
        this.inElection = false
        this.phaseNumber = 0
        this.leftReturned = false
        this.rightReturned = false
    }
}

object HSActor {
    
    import Location._
    import Topic._
    
    object Directions {
        val LEFT: Direction = "A" >> "B"
        val RIGHT: Direction = LEFT.reverse
    }
    
    object Topics {
        val WIN: Topic = "/Win"
        val PROBING: Topic = "/Forward"
        val RETURN: Topic = "/Return"
    }
    
}