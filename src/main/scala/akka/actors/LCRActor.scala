package akka.actors

import akka.actor.{Actor, ActorSelection}
import akka.templates.TemplateActor
import election.Election
import messages.{Message, _}

class LCRActor(method: Election.Method) extends TemplateActor[Content] {
    
    import akka.templates.CustomActor._
    
    val eUID: Int = self.path.uid
    var myLeft: ActorSelection = _
    
    var actualLeader: ActorSelection = _
    var leaderUID: Int = -1
    var amITheLeader = false
    var inElection = false
    
    override def preStart: Unit = {
        super.preStart
        log.debug("My Election ID is {}", eUID)
    }
    
    override protected def init(message: Template[Content]): Unit = {
        super.init(message)
        
        message match {
            case Message(_, _, DiscoverLCR(left)) =>
                log.debug("My left reference is {}", left)
                this.myLeft = left
                unstashAll

            case _ => throw new Exception("Wrong initialization package" + message.toString)
        }
    }
    
    override def receptive: Receive = {
        /**
          * When a crash is detected,
          * then the first process that detects it starts the election procedure
          *
          */
        case Message(_, _, CrashDetected(itsLeft, leader, _)) =>
    
            log.info("Crash of {} detected ...", leader)
            this.inElection = true
            if (leader == myLeft.toSerializationFormat) myLeft = itsLeft
    
            myLeft ! Message(content = ElectionLCR(eUID, self))
        
        case message@Message(_, _, _: Victory) => OUT(message)

        case message@Message(_, _, _: ElectionLCR) =>
    
            LCR(message); context.become(behavior = LCR, discardOld = true)

        case _ => parent forward _
    }
    
    /**
      * Le Lann, Chang and Roberts Leader-election Algorithm
      *
      */
    private val LCR: Actor.Receive = {
        
        /**
          * When a process receive an Election Message of an Election Procedure then
          *
          * A) it forward the election message if the eID on the left process is greater than its eiD
          *
          * B) Else if it's an Election Message of an Election Procedure it's not participating to,
          * then it sends an Election Message for itself to its left process
          *
          * C) Else if it's own Election Message of an Election Procedure it's participating to,
          * then it propagates a Winner Message to its left companion
          *
          * D) Else ignore the message
          *
          */
        case message@Message(_, _, ElectionLCR(reID, _)) =>
    
            parent forward message
            
            if (reID > eUID) {
                myLeft forward message
            } else if (!inElection) {
                this.inElection = true
                myLeft ! Message(content = ElectionLCR(eUID, self))
            } else if (reID == eUID) {
                this.amITheLeader = true
                myLeft ! Message(content = Victory(eUID, self))
            }

        /**
          * When a process receive an Election Win Message then
          *
          * 1) it saves the new leader
          * 2) forward such message to its left process
          * 3) return to phase 1 of the Election Process, waiting for another crash
          */
        case message@Message(_, _, _: Victory) =>
            OUT(message); context.become(behavior = receptive, discardOld = false)

        case _ => parent forward _
    }
    
    val OUT: Receive = {
        
        case message@Message(_, _, Victory(leID, newLeader)) =>
    
            parent forward message
    
            this.log.info("The new leader is {} with election ID#{}", newLeader.toSerializationFormat, leID)
            this.actualLeader = newLeader
    
            if (!amITheLeader) myLeft forward message
    
            parent ! Message(content = End())
            
            this.inElection = false

        case _ => parent forward _
    }
}