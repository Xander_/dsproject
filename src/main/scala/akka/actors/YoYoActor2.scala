package akka.actors

import akka.actor.ActorSelection
import akka.actors.YoYoActor2.{Status, _}
import akka.templates.TemplateActor
import election.Election
import messages._

import scala.collection.immutable.{HashMap, HashSet}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

//noinspection ForwardReference
class YoYoActor2(method: Election.Method) extends TemplateActor[Content] {
    
    import akka.templates.CustomActor._
    import messages.SinkReply.Type._
    
    val eUID: Int = self.path.uid
    var status: Status.Value = Status.GENERIC_NODE
    
    var neighbors: Set[ActorSelection] = HashSet.empty
    var ins: Set[ActorSelection] = HashSet.empty
    var outs: Set[ActorSelection] = HashSet.empty
    var inValues: Map[Int, Set[ActorSelection]] = HashMap.empty.withDefaultValue(Set.empty)
    var sinkMe: Set[SinkType] = Set.empty
    
    var acks: Set[ActorSelection] = Set.empty
    var localMin: Int = 0
    var cycles: Int = 0
    
    var actualLeader: ActorSelection = _
    var amITheLeader: Boolean = false
    
    override protected def init(message: Template[Content]): Unit = {
        super.init(message)
        message match {
            
            case Message(_, _, that: DiscoverYoYo) =>
                log.debug("Local neighbors are {}", that.others)
                neighbors = that.others.mapValues(context.actorSelection).values.toSet
                unstash
            
            case _ => throw new Exception("Wrong initialization package" + message.toString)
        }
    }
    
    override protected def receptive: Receive = {
        
        case Message(_, _, CrashDetected(_, leader, _)) =>
            log.info("Crash of {} detected ...", leader)
            context.become(behavior = SetUp, discardOld = true)
            neighbors.foreach(_ ! Message(Topics.REQUEST, Direction.AnyDir, SetupYoYo(eUID, self)))
        
        case message@Message(Topics.REQUEST, _, _) =>
            context.become(behavior = SetUp, discardOld = true)
            neighbors.foreach(_ ! Message(Topics.REQUEST, Direction.AnyDir, SetupYoYo(eUID, self)))
            SetUp(message);
        
        case _ => parent forward _
    }
    
    /** Yo-Yo ALGORITHM VIRTUAL DAG SETUP ******************************************************************************/
    
    val SetUp: Receive = {
        
        case message@Message(Topics.REQUEST, _, that: SetupYoYo) =>
            parent forward message
            // Answer to all Setup Request Messages with a Setup Response Message which contains this Node ID
            context.actorSelection(that.ref) ! Message(Topics.RESPONSE, Direction.AnyDir, SetupYoYo(eUID, self))
        
        case message@Message(Topics.RESPONSE, _, that: SetupYoYo) =>
            parent forward message
            // Build in and out neighbors orienting logical links
            if (that.eUID > this.eUID) {
                outs = outs + context.actorSelection(that.ref)
            } else {
                ins = ins + context.actorSelection(that.ref)
            }
            
            if (ins.size + outs.size == neighbors.size) {
                
                if (ins.isEmpty) {
                    status = Status.SOURCE_NODE
                    context.become(behavior = SourceNodePhase0, discardOld = true)
                    
                    Future {
                        Thread.sleep(1000L)
                    }.onComplete { _ =>
                        cycles += 1
                        log.info("New Run #{}, Source sending value to {} out-Neighbors", cycles, outs.size)
                        outs.foreach(_ ! Message(content = ElectionYoYo(eUID, self)))
                    }
                    
                } else if (outs.isEmpty) {
                    status = Status.SINK_NODE
                    context.become(behavior = SinkNodePhase0, discardOld = true)
                    
                } else {
                    status = Status.INTERNAL_NODE
                    context.become(behavior = InternalNodePhase1, discardOld = true)
                }
                
                log.info("Status: {}", status.toString)
            }
        
        case _ =>
    }
    
    /** SOURCE NODE BEHAVIOUR ******************************************************************************************/
    
    val SourceNodePhase0: Receive = {
        
        case message@Message(_, _, that: SinkReply) =>
            log.debug("Received {}", that)
            parent forward message
            
            sinkMe += that.reply
            acks += sender.toSerializationFormat
            
            // Prune
            if (that.pruneMe.nonEmpty) {
                outs -= that.pruneMe.head.ref
                log.debug("[{}] Pruned out {}, left {} out neighbors, {}", status, that.pruneMe.head.ref, outs.size,
                    outs.map(_.toSerializationFormat.split('/').reverse.head).mkString(", "))
            }
            
            if ((outs -- acks).isEmpty) {
                // If a source receive at least a NO from its output neighbors then it must become a SINK
                if (sinkMe.contains(SinkReply.Type.NO)) {
                    context.become(behavior = SinkNodePhase0, discardOld = true)
                    log.info("[{}] Received a NO and will become a Sink...", status)
                    status = Status.SINK_NODE
                    outs.foreach {
                        _ ! Message(topic = Topics.SWITCH_INPUT, content = PruneMe(eUID, self))
                    }
                    ins ++= outs
                    outs --= outs
                } else {
                    log.debug("[{}] Received only YES from out-Neighbors...", status)
                    log.debug("Survived out-Neighbors {}", outs.map(_.toSerializationFormat.split('/').reverse.head).mkString(", "))
                    if (outs.isEmpty) {
                        log.info("Victory! I'm the last Source left.")
                        neighbors.foreach(_ ! Message(content = Victory(eUID, self)))
                    } else {
                        Future {
                            Thread.sleep(1000L)
                        }.onComplete { _ =>
                            cycles += 1
                            log.info("New Run #{}, Source sending value to {} out-Neighbors, {}", cycles, outs.size,
                                outs.map(_.toSerializationFormat.split('/').reverse.head).mkString(", "))
                            outs.foreach(_ ! Message(content = ElectionYoYo(eUID, self)))
                        }
                    }
                }
                cycleReset()
                unstashAll()
            } else {
                log.debug("[{}] Waiting for {} more acks", status, (outs -- acks).size)
            }
        
        case message@Message(_, _, _: Victory) => exiting(message)
        
        case _ => stash()
    }
    
    /** INTERNAL NODE BEHAVIOUR ***************************************************************************************/
    
    val InternalNodePhase1: Receive = {
        
        case message@Message(_, _, that: ElectionYoYo) =>
            parent forward message
            inValues += that.eUID -> (inValues(that.eUID) + context.actorSelection(that.ref))
            if (inValues.values.flatten.size >= ins.size) {
                context.become(behavior = InternalNodePhase2, discardOld = true)
                localMin = inValues.keySet.min
                log.debug("Local {} Min {}", status, localMin)
                outs.foreach(_ ! Message(content = ElectionYoYo(localMin, self)))
                unstashAll()
            }
        
        case message@Message(Topics.SWITCH_INPUT, _, input: PruneMe) =>
            parent forward message
            log.info("[{}] Switched {} from input to output...", status, input.ref)
            ins -= input.ref
            outs += input.ref
            
            if (ins.isEmpty) {
                context.become(behavior = SourceNodePhase0, discardOld = true)
                log.info("This {} will become a Source due to its only input becoming an output...", status)
                status = Status.SOURCE_NODE
                cycles += 1
                log.info("New Run #{}, Source sending value to {} out-Neighbors, {}", cycles, outs.size,
                    outs.map(_.toSerializationFormat.split('/').reverse.head).mkString(", "))
                outs.foreach(_ ! Message(content = ElectionYoYo(eUID, self)))
                unstashAll()
            } else if (inValues.values.flatten.size >= ins.size) {
                context.become(behavior = InternalNodePhase2, discardOld = true)
                localMin = inValues.keySet.min
                log.debug("Local {} Min {}", status, localMin)
                outs.foreach(_ ! Message(content = ElectionYoYo(localMin, self)))
                unstashAll()
            }
        
        case message@Message(_, _, _: Victory) => exiting(message)
        
        case _ => stash()
    }
    
    val InternalNodePhase2: Receive = {
        
        case message@Message(_, _, that: SinkReply) =>
            
            log.debug("Received {}", that)
            
            parent forward message
            sinkMe += that.reply
            acks += sender.toSerializationFormat
            
            // Prune Off the node
            if (that.pruneMe.nonEmpty) {
                outs -= that.pruneMe.head.ref
                log.debug("[{}] Pruned out {}, left {} out neighbors, {}", status, that.pruneMe.head.ref, outs.size,
                    outs.map(_.toSerializationFormat.split('/').reverse.head).mkString(", "))
            }
            
            if ((outs -- acks).isEmpty) {
                context.become(behavior = InternalNodePhase1, discardOld = true)
                // Check if this node has become a SINK aka has any output neighbor left
                if (outs.isEmpty) {
                    context.become(behavior = SinkNodePhase0, discardOld = true)
                    log.info("[{}] Has become a Sink", status)
                    status = Status.SINK_NODE
                }
                // Check from which nodes this internal node should be pruned off
                // Those are (all but one) the nodes that have sent the same value
                val pruneMeFrom = inValues.values.filter(_.size > 1).flatMap(set => set - set.head).toSet
                // Copy the remaining node from which you DON'T want to be SURELY pruned from, but you may
                val leftovers = ins -- pruneMeFrom
                // If only 1 input-neighbor is left then we should be pruned totally cause we are a leaf
                val shouldBePrunedAtLast = if (leftovers.size <= 1 && status == Status.SINK_NODE) {
                    context.become(behavior = exiting, discardOld = true)
                    log.info("[{}] Has become a Leaf and will exit...", status)
                    Some(PruneMe(eUID, self))
                } else None
                // If at least a NO has been received then is necessary to send NO to all the input-neighbors
                // Also this node will become useless since all the other nodes connected will spread a NO as well
                if (sinkMe.contains(SinkReply.Type.NO)) {
                    log.info("[Internal Node] Received a NO...")
                    // Then for all those node from which we should be pruned off send a NO message
                    // and our desire to be pruned from them
                    pruneMeFrom.foreach {
                        _ ! Message(content = SinkReply(NO, Some(PruneMe(eUID, self))))
                    }
                    // For all the remaining node send NO and if we should be pruned
                    // (only 1 input neighbor left and this node is a sink)
                    leftovers.foreach {
                        _ ! Message(content = SinkReply(NO, shouldBePrunedAtLast))
                    }
                } else {
                    log.debug("[{}] Received only YES from out-Neighbors...", status)
                    // The node should send yes only to those nodes that have sent the local min
                    // First do this for the nodes from which we should be pruned off
                    pruneMeFrom.foreach(in => {
                        val reply = if (inValues(localMin).contains(in)) YES else NO
                        in ! Message(content = SinkReply(reply, Option(PruneMe(eUID, self))))
                    })
                    // Then for those left do the same but send if you should be pruned at last,
                    // that is, you got only 1 in neighbor left
                    leftovers.foreach(in => {
                        val reply = if (inValues(localMin).contains(in)) YES else NO
                        in ! Message(content = SinkReply(reply, shouldBePrunedAtLast))
                    })
                }
                // The new input neighbors are the leftover ones
                // If the leftovers input node that have survived pruning are only 1
                // and this node has become a Sink then die.
                ins = if (shouldBePrunedAtLast.isEmpty) ins.empty else leftovers
                
                cycleReset()
                unstashAll()
                
            } else {
                log.debug("[{}] Waiting for {} more acks", status, (outs -- acks).size)
            }
        
        case _ => stash()
    }
    
    /** SINK NODE BEHAVIOUR *******************************************************************************************/
    
    val SinkNodePhase0: Receive = {
        
        case message@Message(Topics.SWITCH_INPUT, _, input: PruneMe) =>
            parent forward message
            log.info("[{}] Has become an Internal Node...", status)
            status = Status.INTERNAL_NODE
            context.become(behavior = InternalNodePhase1, discardOld = true)
            
            ins -= input.ref
            outs += input.ref
            if (ins.isEmpty) {
                context.become(behavior = SourceNodePhase0, discardOld = true)
                log.info("This {} will become a Source due to its only input becoming an output...", status)
                status = Status.SOURCE_NODE
                cycles += 1
                log.info("New Run #{}, Source sending value to {} out-Neighbors, {}", cycles, outs.size,
                    outs.map(_.toSerializationFormat.split('/').reverse.head).mkString(", "))
                outs.foreach(_ ! Message(content = ElectionYoYo(eUID, self)))
                unstashAll()
            } else if (inValues.values.flatten.size >= ins.size) {
                
                context.become(behavior = InternalNodePhase2, discardOld = true)
                localMin = inValues.keySet.min
                log.debug("Local {} Min {}", status, localMin)
                outs.foreach(_ ! Message(content = ElectionYoYo(localMin, self)))
                unstashAll()
            }
        
        case message@Message(_, _, that: ElectionYoYo) =>
            parent forward message
            inValues += that.eUID -> (inValues(that.eUID) + context.actorSelection(that.ref))
            if (inValues.values.flatten.size >= ins.size) {
                localMin = inValues.keySet.min
                log.debug("Local {} Min {}", status, localMin)
                // This collection will contains only those node from which this node should be pruned off
                // aka all those node that have sent the same value except one
                val pruneMeFrom = inValues.values.filter(_.size > 1).flatMap(set => set - set.head).toSet
                // Copy the remaining node from which you DON'T have to be SURELY pruned, but you may
                val leftovers = ins -- pruneMeFrom
                // Then Evaluate if this sink is left with only 1 input-neighbor, that is, it has become a leaf
                val shouldBePrunedAtLast = if (leftovers.size <= 1) {
                    log.info("[{}] Has become a leaf and will exit...", status)
                    // If the sink has become a leaf then move to the exiting behaviour
                    context.become(behavior = exiting, discardOld = true)
                    Some(PruneMe(eUID, self))
                } else None
                // First reply YES/NO to those chosen to be pruned from
                pruneMeFrom.foreach(in => {
                    val reply = if (inValues(localMin).contains(in)) YES else NO
                    log.debug("reply: {} to {}", reply, in.toSerializationFormat)
                    in ! Message(content = SinkReply(reply, Some(PruneMe(eUID, self))))
                })
                // Send the message to all the remaining input neighbors (1 or more)
                leftovers.foreach(in => {
                    val reply = if (inValues(localMin).contains(in)) YES else NO
                    log.debug("reply: {} to {}", reply, in.toSerializationFormat.split('/').reverse.head)
                    in ! Message(content = SinkReply(reply, shouldBePrunedAtLast))
                })
                // Remove from the input neighbors all those nodes from which you have been pruned off
                // that is, the remaining neighbors are the leftover ones
                ins = if (shouldBePrunedAtLast.isEmpty) ins.empty else leftovers
                // Reset stuff of this cycle and the un-stash unhandled messages
                cycleReset()
                unstashAll()
            }
        
        case message@Message(_, _, _: Victory) => exiting(message)
        
        case _ => stash()
    }
    
    /** ****************************************************************************************************************/
    
    val exiting: Receive = {
        case message@Message(_, _, leader: Victory) =>
            
            context.become(behavior = receptive, discardOld = true)
            this.log.info("The new leader is {} with election ID#{}",
                leader.ref.toSerializationFormat.split('/').reverse.head, leader.eUID)
            this.actualLeader = leader.ref
            if (!amITheLeader) neighbors.foreach(_ ! message)
            this.reset()
            
            parent forward message
            parent ! Message(content = End(cycles + 1))
            
            unstashAll()
        
        case _ => stash()
    }
    
    private def reset(): Unit = {
        this.outs = outs.empty
        this.ins = ins.empty
        this.cycles = 0
        this.status = Status.GENERIC_NODE
        cycleReset()
    }
    
    private def cycleReset(): Unit = {
        localMin = 0
        acks --= acks
        sinkMe --= sinkMe
        inValues = inValues.empty.withDefaultValue(Set.empty)
    }
}

object YoYoActor2 {
    
    object Status extends Enumeration {
        val GENERIC_NODE: Status.Value = Value(-1, "Simple Node")
        val SOURCE_NODE: Status.Value = Value(0, "Source Node")
        val SINK_NODE: Status.Value = Value(1, "Sink Node")
        val INTERNAL_NODE: Status.Value = Value(2, "Internal Node")
    }
    
    object Topics {
        val REQUEST: Topic = "/Request"
        val RESPONSE: Topic = "/Response"
        val SWITCH_INPUT: Topic = "/RemoveInputNode"
        val PERFORM_SINK: Topic = "/Sink"
    }
    
}