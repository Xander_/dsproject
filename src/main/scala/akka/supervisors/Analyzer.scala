package akka.supervisors

import akka.actor.Actor
import akka.http.scaladsl.model.DateTime
import akka.templates.CustomActor
import election.Election
import messages.{Content, Empty, End, Message}

import scala.collection.mutable

class Analyzer(N: Int, method: Election.Method) extends CustomActor {
    
    var startTime: DateTime = DateTime.now
    var exchangedMessages: mutable.Map[String, Int] = mutable.HashMap.empty.withDefaultValue(0)
    var expectedEndMsg = 0
    var rounds = 1
    
    override def receive: Actor.Receive = {
    
        case Message(_, _, e: End) =>
    
            rounds = Math.max(e.rounds, rounds)
    
            expectedEndMsg += 1
    
            if (expectedEndMsg == N) {
                log.info("Started @ {}, Completed @ {}", startTime, DateTime.now)
                log.info("Number of rounds executed #{}", rounds)
                log.info("Number of sent messages #{}", exchangedMessages.values.sum)
                exchangedMessages.foreach {
                    case (k, v) => log.debug("Message {} sent {} times", k, v)
                }
                rounds = 1
                exchangedMessages.clear
                expectedEndMsg = 1
            }

        case Message(_, _, cnt: Content) =>
    
            if (method == Election.LCR && cnt.isInstanceOf[messages.ElectionLCR]) rounds += 1
            if (method == Election.Bully && cnt.isInstanceOf[messages.ElectionBully]) rounds += 1
            exchangedMessages += cnt.getClass.getSimpleName -> (exchangedMessages(cnt.getClass.getSimpleName) + 1)

        case time: DateTime => this.startTime = time
        case _ =>
    }
}
