package messages

import akka.actors.HSActor.{Directions, Topics}
import spray.json.{DefaultJsonProtocol, _}

object Format extends DefaultJsonProtocol {
    
    implicit val EmptyMessageFormat: JsonFormat[Empty] = jsonFormat0(Empty.apply)
    implicit val GreetingsMessageFormat: JsonFormat[Greetings] = jsonFormat1(Greetings.apply)
    implicit val EndMessageFormat: JsonFormat[End] = jsonFormat1(End.apply)
    
    implicit val DiscoverLCRMessageFormat: JsonFormat[DiscoverLCR] = jsonFormat1(DiscoverLCR.apply)
    implicit val DiscoverHSMessageFormat: JsonFormat[DiscoverHS] = jsonFormat3(DiscoverHS.apply)
    implicit val DiscoverBullyMessageFormat: JsonFormat[DiscoverBully] = new JsonFormat[DiscoverBully] {
        override def read(json: JsValue): DiscoverBully = {
            DiscoverBully(json.asJsObject.fields.toList.head._2.convertTo[Map[Int, String]])
        }
        
        override def write(obj: DiscoverBully): JsValue = new JsObject(Map("others" -> obj.others.toJson))
    }
    
    implicit val DiscoverYoYoMessageFormat: JsonFormat[DiscoverYoYo] = new JsonFormat[DiscoverYoYo] {
        override def read(json: JsValue): DiscoverYoYo = {
            DiscoverYoYo(json.asJsObject.fields.toList.head._2.convertTo[Map[Int, String]])
        }
        
        override def write(obj: DiscoverYoYo): JsValue = new JsObject(Map("others" -> obj.others.toJson))
    }
    
    implicit val ElectionLCRMessageFormat: JsonFormat[ElectionLCR] = jsonFormat2(ElectionLCR.apply)
    implicit val ElectionHSMessageFormat: JsonFormat[ElectionHS] = jsonFormat3(ElectionHS.apply)
    implicit val ElectionBullyMessageFormat: JsonFormat[ElectionBully] = jsonFormat2(ElectionBully.apply)
    implicit val ElectionYoYoMessageFormat: JsonFormat[ElectionYoYo] = jsonFormat2(ElectionYoYo.apply)
    implicit val SetupYoYoMessageFormat: JsonFormat[SetupYoYo] = jsonFormat2(SetupYoYo.apply)
    implicit val PruneMeMessageFormat: JsonFormat[PruneMe] = jsonFormat2(PruneMe.apply)
    implicit val SinkResponseMessageFormat: JsonFormat[SinkReply] = jsonFormat2(SinkReply.apply)
    
    implicit val VictoryMessageFormat: JsonFormat[Victory] = jsonFormat2(Victory.apply)
    implicit val ReplyVictoryMessageFormat: JsonFormat[ReplyVictory] = jsonFormat2(ReplyVictory.apply)
    
    implicit val SimulateCrashMessageFormat: JsonFormat[SimulateCrash] = jsonFormat1(SimulateCrash.apply)
    implicit val SimulateAwakeMessageFormat: JsonFormat[SimulateAwake] = jsonFormat1(SimulateAwake.apply)
    implicit val CrashDetectedMessageFormat: JsonFormat[CrashDetected] = jsonFormat3(CrashDetected.apply)
    
    implicit val TopicMessageFormat: JsonFormat[TopicImpl] = lazyFormat(jsonFormat(TopicImpl, "topicName", "parentTopic"))
    
    implicit val TopicTraitFormat: JsonFormat[Topic] = new JsonFormat[Topic] {
        override def read(json: JsValue): Topic = json.convertTo[TopicImpl]
        
        override def write(obj: Topic): JsValue = TopicImpl(obj.topicName, obj.parentTopic).toJson
    }
    
    implicit val LocationFormat: JsonFormat[LocationImpl] = jsonFormat1(LocationImpl)
    
    implicit val LocationTraitFormat: JsonFormat[Location] = new JsonFormat[Location] {
        override def read(json: JsValue): Location = json.convertTo[LocationImpl]
        
        override def write(obj: Location): JsValue = LocationImpl(obj.loc).toJson
    }
    
    implicit val DirectionMessageFormat: JsonFormat[DirectionImpl] = jsonFormat2(DirectionImpl)
    
    implicit val DirectionTraitFormat: JsonFormat[Direction] = new JsonFormat[Direction] {
        override def read(json: JsValue): Direction = json.convertTo[DirectionImpl]
        
        override def write(obj: Direction): JsValue = DirectionImpl(obj.from, obj.to).toJson
    }
    
    implicit def selectContentUnmarshaller(manifest: JsString)(content: JsValue): Content = manifest.value match {
        
        case man if man == classOf[Greetings].getName => content.convertTo[Greetings]
        case man if man == classOf[End].getName => content.convertTo[End]
        case man if man == classOf[DiscoverLCR].getName => content.convertTo[DiscoverLCR]
        case man if man == classOf[DiscoverHS].getName => content.convertTo[DiscoverHS]
        case man if man == classOf[ElectionLCR].getName => content.convertTo[ElectionLCR]
        case man if man == classOf[ElectionHS].getName => content.convertTo[ElectionHS]
        case man if man == classOf[Victory].getName => content.convertTo[Victory]
        case man if man == classOf[SimulateCrash].getName => content.convertTo[SimulateCrash]
        case man if man == classOf[CrashDetected].getName => content.convertTo[CrashDetected]
        
        case _ => Empty()
    }
    
}
object TryMessageFormatting extends App {
    
    import Format._
    
    val c1 = Greetings(List("Ciao", "Hello", "Allo"))
    val c2 = ElectionHS(0, "/Akka/DSP/System/Actor/123", 0)
    println(c1.toJson)
    println(c2.toJson)
    
    val topic: Topic = "/Init/Greetings"
    
    println(topic.toJson)
    
    println(topic.toJson.convertTo[Topic])
    
    val msg = Message(Topics.PROBING, Directions.LEFT, c2)
    
    val serializer: Serializer = new messages.Serializer()
    val serialized = serializer.toBinary(msg)
    
    println(serialized.toStream.map(_.toChar).mkString)
    
    println(serializer.fromBinary(serialized, serializer.manifest(msg)))
}

