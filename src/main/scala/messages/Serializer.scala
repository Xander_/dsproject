package messages

import akka.serialization.SerializerWithStringManifest
import spray.json._
import Format._

/**
  * A Custom Serializer for Message(s) to be handled by the ActorSystem itself
  *
  */
class Serializer extends SerializerWithStringManifest {
    
    override def identifier = 21040507
    
    override def manifest(obj: AnyRef): String = obj match {
        case msg: Message[Content] => msg.getClass.getName
        case msg: Template[Content] => msg.getClass.getName
        case _ => null
    }
    
    override def toBinary(obj: AnyRef): Array[Byte] = obj match {
        case msg: Message[Content] => MessageSerializer.serialize(msg)
        case msg: Template[Content] => MessageSerializer.serialize(msg)
        case _ => null
    }
    
    override def fromBinary(bytes: Array[Byte], manifest: String): AnyRef = manifest match {
        case man if man == classOf[Message[Content]].getName => MessageSerializer.deserialize(bytes)
        case man if man == classOf[Template[Content]].getName => MessageSerializer.deserialize(bytes)
        case _ => null
    }
    
    
}

/**
  * A Custom Serializer that defines methods for an Object to be Serialized.
  *
  * The actual implementation is statically provided by the Object companion MessageSerializer
  *
  */
trait MessageSerializer[C] {
    
    /**
      * Performs a transformation of the given message from its object view to an array of Byte
      *
      * @param message The Message object to be serialized
      * @return The Array of  Byte representing the object
      */
    def serialize(message: messages.Template[C]): Array[Byte]
    
    /**
      * Performs a transformation of the given message from a Byte Array to its Object view
      *
      * @param bytes The Array of Byte representing the Message
      * @return The object view of the Message
      */
    def deserialize(bytes: Array[Byte]): messages.Template[C]
}

/**
  * An Utility Companion Object that provides the logic to Serialize any object that implements the trait Message
  */
object MessageSerializer extends MessageSerializer[Content] {
    
    override def serialize(msg: messages.Template[Content]): Array[Byte] = {
        JsObject(
            Map(
                "topic" -> msg.topic.toJson,
                "direction" -> msg.direction.toJson,
                "content" -> msg.content.marshal.toJson,
                "metadata" -> msg.content.getClass.getName.toJson
            )
        ).toString.getBytes
    }
    
    override def deserialize(bytes: Array[Byte]): messages.Template[Content] = {
        bytes.toStream.map(_.toChar).mkString.parseJson.asJsObject.fields.values.toList match {
            case List(topic: JsObject, direction: JsObject, content: JsObject, metadata: JsString) =>
                Message(
                    topic.convertTo[Topic],
                    direction.convertTo[Direction],
                    selectContentUnmarshaller(metadata)(content)
                )
            case _ => throw DeserializationException("Unknown Exception")
        }
    }
    
}