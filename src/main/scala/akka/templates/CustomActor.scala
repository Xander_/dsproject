package akka.templates

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSelection, Stash}

import scala.language.postfixOps

/**
  * A CustomActor that ease the use of few overused methods
  *
  * Created by Alessandro on 01/07/2017.
  */
trait CustomActor extends Actor with Stash with ActorLogging {
    
    implicit def ActorRefToActorSelection(ref: ActorRef): ActorSelection = context.actorSelection(ref.path.toSerializationFormat)
    
    implicit def StringToActorSelection(str: String): ActorSelection = context.actorSelection(str)
    
    /**
      * A shortcut method that gives the name of this actor
      *
      * @return The name of this actor
      */
    def name: String = self.path.name
    
    /**
      * A shortcut method that gives the ArctorRef of the parent Actor of this Actor
      *
      * @return The parent ActorRef
      */
    def parent: ActorRef = this.context.parent
    
    /**
      * A shortcut method that gives the ActorSelection of the Sibling with the
      * specified name, if exist
      *
      * @param name The name of the sibling
      * @return Return an Option[ActorSelection] of the found sibling
      */
    def sibling(name: String): Option[ActorSelection] =
        Option(context.actorSelection(parent.path / name))
    
    /**
      * A shortcut method that gives all the siblings of this Actor
      *
      * @return Return all the siblings of this node including itself
      */
    def siblings: ActorSelection = context.actorSelection(parent.path / "*")
    
    /**
      * A shortcut method that gives the ActorSelection of the Child with the specified name
      *
      * @param name The name of the wanted child
      * @return An Option[ActorSelection] of the found child
      */
    def child(name: String): Option[ActorSelection] =
        Option(context.actorSelection(self.path / name))
    
    /**
      * A shortcut method that gives all the children of this Actor
      *
      * @return All the children of this Actor
      */
    def children: ActorSelection = context.actorSelection(self.path / "*")
    
}

object CustomActor {
    
    implicit def ActorRefToString(ref: ActorRef): String = ref.path.toSerializationFormat
    
    implicit def ActorSelectionToString(sel: ActorSelection): String = sel.toSerializationFormat
}

