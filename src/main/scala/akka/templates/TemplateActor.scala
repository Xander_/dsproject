package akka.templates

import messages.Template

/**
  * An actual implementation of CustomActor with the addition of few protected method
  * that define the base behaviour of such Actor.
  *
  * This kind of Actor is supposed to be used for Actors that need an Initialization
  * or need wait before becoming fully operative.
  *
  * <b>NOTE</b>: The concept used here is one of a <u>Template Pattern</u>, this is the provided template,
  * The implementation only provide the effective behaviour.
  *
  * Created by Alessandro on 01/07/2017.
  */
abstract class TemplateActor[I] extends CustomActor {
    
    override def preStart: Unit = {
        log.debug("Hello there, I need to be initialized!")
    }
    
    override def receive: Receive = resistive
    
    /**
      * The resistive method is the default behaviour of the BasicActor
      * where it waits for an InitMessage.
      *
      * Can be overridden if needed but remember to call this old behaviour at the end of the case match.
      *
      * Like this:
      * case 1 => Do stuff
      * case 2 => Do other stuff
      * ...
      * case msg => super.resistive(msg)
      *
      * @return An Actor Receive Behaviour
      */
    protected def resistive: Receive = {
        case msg: Template[I] =>
            try {
                this.init(msg)
                this.context.become(receptive, discardOld = true)
                log.debug("I've become receptive!")
            } catch {
                case ex: Throwable => {
                    log.error(ex.getMessage)
                    stash()
                }
            }
        case _ => desist _
    }
    
    /**
      * This is the default init for the BasicActor, it simply log a message of self existence.
      *
      * To override if particular Initialization is need, that is the List of arguments have to be used.
      *
      * <b>IMPORTANT NOTE</b>: Always remember to call un-stash at end when overridden
      *
      * @param msg The message that contains a list of arguments needed to initialize post creation this Actor.
      */
    protected def init(msg: Template[I]): Unit = {
        log.debug("Initialized, {}!", name)
    }
    
    /**
      * This method is the behaviour on which the BasicActor will be
      * after the Initialization has been completed.
      *
      * @return An Actor Receive Behaviour
      */
    protected def receptive: Receive
    
    /**
      * This method is the default action to be used for unhandled messages
      *
      * @param msg The unhandled message from the current behaviour
      */
    protected def desist(msg: Any): Unit = log.debug("Unhandled... {}", msg.toString) // Ignore
}
