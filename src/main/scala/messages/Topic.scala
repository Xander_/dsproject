package messages

/**
  * A simple Topic
  *
  * Created by Xander_C on 03/07/2017.
  */
trait Topic {
    
    /**
      * The name of this Topic
      */
    val topicName: String
    
    /**
      * The Subtopic of which this Topic is a Super-topic
      */
    val parentTopic: Option[Topic]
    
    /**
      * The long name of this topic by composing topic and subtopic names
      *
      * @return The composition of topic and subtopic names
      */
    def longName: String = {
        String.join("/", parentTopic.getOrElse("").toString, topicName)
    }
    
    override def toString: String = longName
    
    override def equals(obj: scala.Any): Boolean = obj match {
        case o: Topic => o.longName == this.longName
        case o: String => o == this.longName
    }
}

case class TopicImpl(topicName: String, parentTopic: Option[Topic]) extends Topic

object Topic {
    
    val AnyTopic: Topic = TopicImpl("Any", Option.empty)
    
    implicit def Topic2String(msg: Topic): String = msg.topicName
    
    implicit def String2Topic(str: String): Topic = {
        
        def go(l: List[String]): Option[Topic] = {
            if (l.isEmpty) Option.empty
            else Option(TopicImpl(l.head, go(l.tail)))
        }
        
        val elems = str.split("/").filter(_ != "").toList.reverse
        TopicImpl(elems.head, go(elems.tail))
    }
}

object TryTopic extends App {
    
    val t = TopicImpl(
        "Cell",
        Option(
            TopicImpl(
                "Discovery",
                Option(
                    TopicImpl(
                        "Init",
                        Option.empty
                    )
                )
            )
        )
    )
    
    val t2: Topic = "/Closure/ACK"
    
    println(t)
    
    println(t.longName)
    
    println(t2)
    
}
