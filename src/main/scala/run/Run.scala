package run

import java.io.File

import akka.actor.{ActorSystem, Props}
import akka.actors.Master
import com.typesafe.config.{Config, ConfigFactory}
import election.Election
import messages._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object Run extends App {
    
    val N = 10
    val electionMethod = Election.YoYo
    
    implicit val config: Config = ConfigFactory.parseFile(new File("res/akka/master.conf"))
        .withFallback(ConfigFactory.load())
        .resolve()
    
    implicit val actorSystem: ActorSystem = ActorSystem("DSP", config)
    
    val master = actorSystem.actorOf(Props(new Master(N, electionMethod)), "Master")
    
    master ! Message(content = Empty())
    
    Future {
        Thread.sleep(500L)
    }.onComplete(_ => {
        master ! Message(content = SimulateCrash(1))
    })
    
    //        if (electionMethod == Election.Bully) {
    //            Future {
    //                val countdown = 150L
    //                for {i <- 1L until countdown} {
    //                    Thread.sleep(i * 100L)
    //                }
    //            } onComplete (_ => {
    //                master ! Message(content = SimulateAwake())
    //            })
    //        }
}
