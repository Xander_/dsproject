package messages

/**
  * This trait represent a generic message with various header information,
  * like Type and Subtype of the message and the direction.
  *
  * In the end there is a generic Content of type C.
  *
  * Created by Matteo Gabellini and Alessandro Cevoli on 28/06/2017.
  */
trait Template[C] {
    
    def topic: Topic
    
    def direction: Direction
    
    def content: C
    
    override def toString: String =
        "Message on Topic " + topic.toString + ", " + direction.toString + ", carrying \"" + content.toString + "\""
    
    override def equals(obj: Any): Boolean = obj match {
        case msg: Template[_] =>
            msg.topic == this.topic && msg.content == this.content
        case _ => false
    }
}

/**
  * This case class is an actual implementation of the trait Message
  *
  * @param topic     The Supertype Associated with this message
  * @param direction The Direction of this message
  * @param content   The content of this message
  * @tparam C The Type of the content
  */
final case class Message[C](topic: Topic = Topic.AnyTopic,
                            direction: Direction = Direction.AnyDir,
                            content: C) extends Template[C]