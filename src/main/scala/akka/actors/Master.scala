package akka.actors

import akka.actor.{Actor, ActorRef, Props}
import akka.http.scaladsl.model.DateTime
import akka.supervisors.Analyzer
import akka.templates.{CustomActor, TemplateActor}
import election.Election
import messages._

import scala.collection.mutable
import scala.util.Random

class Master(N: Int, method: Election.Method) extends TemplateActor[Content] {
    
    import CustomActor._
    
    val cluster: collection.mutable.Map[Int, ActorRef] = mutable.HashMap.empty
    
    val analyzer: ActorRef = context.actorOf(Props(new Analyzer(N, method)), "Statistics")
    
    var time: DateTime = _
    
    override def preStart: Unit = {
        super.preStart
    
        log.info("Selection mode is {}", method)
        
        for (x <- 0 until N) {
    
            cluster += x -> (method match {
                case Election.LCR =>
                    context.actorOf(Props(new LCRActor(method)), "T" + x)

                case Election.HS =>
                    context.actorOf(Props(new HSActor(method)), "T" + x)

                case Election.Bully =>
                    context.actorOf(Props(new BullyActor(method)), "T" + x)

                case Election.YoYo =>
                    context.actorOf(Props(new YoYoActor2(method)), "T" + x)
                
                case _ => context.actorOf(Props[Actor], "T" + x)
            })
    
            log.debug("{}", x -> cluster(x).toSerializationFormat)
        }
    
        log.debug("Expected Leaders in order: \n\t{}", cluster.values.toList.sortBy(_.path.uid).reverse.mkString("\n\t"))
    }
    
    override protected def init(msg: Template[Content]): Unit = {
        super.init(msg)
    
        val net1 = Map(
            0 -> Set(5, 9),
            1 -> Set(2, 3, 4, 7),
            2 -> Set(1, 5, 6),
            3 -> Set(1, 4),
            4 -> Set(1, 3, 7, 9),
            5 -> Set(2, 9, 0),
            6 -> Set(2, 7, 8),
            7 -> Set(4, 6, 1),
            8 -> Set(6),
            9 -> Set(4, 5, 0)
        )
        val net2 = Map(
            0 -> Set(2, 4),
            1 -> Set(2, 4, 3),
            2 -> Set(1, 0),
            3 -> Set(1),
            4 -> Set(0, 1)
        )
        
        cluster.foreach(e => {
            e._2 ! Message(
                Topic.AnyTopic,
                Direction.AnyDir,
                method match {
                    case Election.LCR =>
                        DiscoverLCR(cluster((e._1 - 1 + N) % N))
                    case Election.HS =>
                        DiscoverHS(cluster((e._1 - 1 + N) % N), cluster((e._1 + 1) % N), cluster.size)
                    case Election.Bully =>
                        DiscoverBully(cluster.map(v => v._2.path.uid -> v._2.toSerializationFormat).toMap)
                    case Election.YoYo =>
                        DiscoverYoYo(
                            //                            Map(
                            //                                cluster((e._1 - 1 + N) % N).path.uid -> cluster((e._1 - 1 + N) % N),
                            //                                cluster((e._1 + 1) % N).path.uid -> cluster((e._1 + 1) % N)
                            //                            )
                            cluster
                                //                            .filter(v => net2(e._1).contains(v._1))
                            .filter(v => net1(e._1).contains(v._1))
                                //                            .filter(v => v._2.path.uid != e._2.path.uid)
                                .map(v => v._2.path.uid -> v._2.toSerializationFormat).toMap
                        )
                    case _ =>
                }
            
            )
        })
    }
    
    override protected def receptive: Receive = {
    
        case s: String => println(s)
    
        case msg@Message(_, _, _: SimulateAwake) =>
            cluster.values.maxBy(_.path.uid) !
                msg.copy(
                    content = SimulateAwake(cluster.map(e => e._2.path.uid -> e._2.toSerializationFormat).toMap)
                )
    
        case msg@Message(_, _, SimulateCrash(mode)) =>
            analyzer ! DateTime.now
            method match {
                case Election.Bully => cluster.values.maxBy(_.path.uid) ! msg
                case _ => mode match {
                    case 1 =>
                        cluster(Random.nextInt(N)) !
                            msg.copy(content = CrashDetected(self, self.toSerializationFormat, self))
                    case 2 =>
                        cluster.values.foreach(_ !
                            msg.copy(content = CrashDetected(self, self.toSerializationFormat, self))
                        )
                    case _ =>
                }
            }
    
        case msg@Message(_, _, _: Content) => analyzer forward msg
        
        case _ =>
    }
}