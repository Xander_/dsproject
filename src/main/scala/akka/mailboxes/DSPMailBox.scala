package akka.mailboxes

import akka.actor.{ActorSystem, PoisonPill}
import akka.dispatch.{PriorityGenerator, UnboundedStablePriorityMailbox}
import com.typesafe.config.Config
import messages.{Message, _}

// We inherit, in this case, from UnboundedStablePriorityMailbox
// and seed it with the priority generator
class DSPMailBox(settings: ActorSystem.Settings, config: Config)
    extends UnboundedStablePriorityMailbox(
        PriorityGenerator {
            case Message(_, _, _: PruneMe) => 0
            case PoisonPill => 2
            case _ => 1
        })
