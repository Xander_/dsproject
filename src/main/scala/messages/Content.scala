package messages

import messages.Content.HookReference
import spray.json._
import Format._
import messages.SinkReply.Type.SinkType


/**
  * A trait that represent a Content for Ariadne Messages
  *
  */
trait Content {
    
    def marshal: JsValue
    
    override def toString: String = marshal.toString
}

object Content {
    type HookReference = String // ActorSelection
}

final case class Empty() extends Content {
    override def marshal: JsValue = this.toJson
}

final case class Greetings(args: List[String]) extends Content {
    override def marshal: JsValue = this.toJson
}

final case class End(rounds: Int = 0) extends Content {
    override def marshal: JsValue = this.toJson
}

final case class DiscoverLCR(left: HookReference) extends Content {
    override def marshal: JsValue = this.toJson
}

final case class DiscoverHS(left: HookReference, right: HookReference, cardinality: Int) extends Content {
    override def marshal: JsValue = this.toJson
}

final case class DiscoverBully(others: Map[Int, HookReference]) extends Content {
    override def marshal: JsValue = this.toJson
}

final case class DiscoverYoYo(others: Map[Int, HookReference]) extends Content {
    override def marshal: JsValue = this.toJson
}

final case class ElectionLCR(eUID: Int, ref: HookReference) extends Content {
    override def marshal: JsValue = this.toJson
}

final case class ElectionHS(eUID: Int, ref: HookReference, hops: Int) extends Content {
    override def marshal: JsValue = this.toJson
}

final case class ElectionBully(eUID: Int, ref: HookReference) extends Content {
    override def marshal: JsValue = this.toJson
}

final case class SetupYoYo(eUID: Int, ref: HookReference) extends Content {
    override def marshal: JsValue = this.toJson
}

final case class ElectionYoYo(eUID: Int, ref: HookReference) extends Content {
    override def marshal: JsValue = this.toJson
}

final case class PruneMe(eUID: Int, ref: HookReference) extends Content {
    override def marshal: JsValue = this.toJson
}

case class SinkReply(reply: SinkType,
                     //                     localMin: Option[Int],
                     pruneMe: Option[PruneMe]) extends Content {
    override def marshal: JsValue = this.toJson
}

object SinkReply {
    
    object Type {
        type SinkType = String
        val YES: SinkType = "Yes"
        val NO: SinkType = "No"
    }
    
}

final case class Victory(eUID: Int, ref: HookReference) extends Content {
    override def marshal: JsValue = this.toJson
}

final case class ReplyVictory(eUID: Int, ref: HookReference) extends Content {
    override def marshal: JsValue = this.toJson
}

final case class CrashDetected(left: HookReference = "", that: HookReference, right: HookReference = "") extends Content {
    override def marshal: JsValue = this.toJson
}

final case class SimulateCrash(mode: Int) extends Content {
    override def marshal: JsValue = this.toJson
}

final case class SimulateAwake(others: Map[Int, HookReference] = Map.empty) extends Content {
    override def marshal: JsValue = this.toJson
}