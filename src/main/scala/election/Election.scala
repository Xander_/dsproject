package election

object Election {
    
    type Method = String
    
    val LCR: Method = "LCR" // Randomized ID LCR using long ID random Identifier
    val HS: Method = "HS" // Phase based LCR
    val Bully: Method = "Bully" // Synchronous
    val Peterson: Method = "Peterson" // Phase Based with Probe
    val YoYo: Method = "Yo-Yo"
}
