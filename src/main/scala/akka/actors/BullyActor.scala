package akka.actors

import java.util.concurrent.TimeUnit

import akka.actor.ActorSelection
import akka.actors.BullyActor._
import akka.pattern.ask
import akka.templates.TemplateActor
import akka.util.Timeout
import election.Election
import messages._

import scala.collection.immutable.HashMap
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Random

//noinspection ForwardReference
class BullyActor(method: Election.Method) extends TemplateActor[Content] {
    
    import akka.templates.CustomActor._
    
    implicit val timeout: Timeout = Timeout(1000L, TimeUnit.MILLISECONDS)
    
    val eUID: Int = self.path.uid
    
    var others: Map[Int, ActorSelection] = HashMap.empty
    var expectedLeaders: List[ActorSelection] = List.empty
    var leadIterator: Iterator[ActorSelection] = expectedLeaders.iterator
    
    var actualLeader: ActorSelection = _
    var leaderUID: Int = -1
    var amITheLeader = false
    var amIInElection = false
    var amIOutOfOrder = false
    var amICrashed = false
    
    override protected def init(message: Template[Content]): Unit = {
        
        super.init(message)
        
        message match {
            case Message(_, _, DiscoverBully(refs: Map[Int, String])) =>
    
                log.debug("My left reference is {}", refs.toString)
    
                others = others ++ HashMap(refs.mapValues(context.actorSelection).toList: _*)
                expectedLeaders = expectedLeaders ++ others.filterKeys(k => k > eUID).values.toList
    
                if (expectedLeaders.isEmpty) this becomeLeaderOf others.valuesIterator
    
                unstashAll

            case _ => throw new Exception("Wrong initialization package" + message.toString)
        }
    }
    
    override protected def receptive: Receive = {
        
        case msg@Message(_, _, s: SimulateAwake)
            if this.amICrashed && expectedLeaders.isEmpty =>
            log.info("Simulating online return...")
            this.init(msg.copy(content = DiscoverBully(s.others)))
        
        case message@Message(_, _, SimulateCrash(mode)) =>
            log.info("Simulating crash in mode {}", mode)
            amITheLeader = false
            amICrashed = true
            
            mode match {
                case 1 => others.filterKeys(_ != this.eUID).apply(
                    others.filterKeys(_ != this.eUID).keys.toList(Random.nextInt(others.size - 1))
                ) ! message.copy(content = CrashDetected(that = self.toSerializationFormat))
                
                case _ => others.filterKeys(_ != this.eUID).values.foreach(_ !
                    message.copy(content = CrashDetected(that = self.toSerializationFormat))
                )
            }
        
        case Message(_, _, CrashDetected(_, leader, _)) =>
    
            log.info("Crash of {} detected...", leader)
    
            this.leadIterator = expectedLeaders.iterator
            contactOf(others.valuesIterator, leadIterator)
            context.become(behavior = Bully, discardOld = true)

        case message@Message(_, _, _: Victory) =>
            //            context.become(behavior = resistive, discardOld = true)
            OUT(message)
        
        case message@Message(topic, _, cnt: Content)
            if (topic == Topics.NEXT || topic == Topics.STOP ||
                cnt.isInstanceOf[ElectionBully]) && !amICrashed =>
            this.leadIterator = expectedLeaders.iterator
            Bully(message)
            context.become(behavior = Bully, discardOld = true)
        
        case _ => parent forward _
    }
    
    /**
      * Leader Election Bully Algorithm
      *
      */
    val Bully: Receive = {
        
        case message@Message(_, _, that: ElectionBully) =>
    
            if (this.eUID > that.eUID) {
    
                parent ! Message(Topics.STOP, Direction.AnyDir, Empty())
                context.actorSelection(that.ref) ! Message(Topics.STOP, Direction.AnyDir, Empty())
                
                if (!amIInElection) {
                    amIInElection = true
                    contactOf(others.valuesIterator, leadIterator)
                }
            }

        case message@Message(Topics.NEXT, _, _) =>
            log.info("Contact with has failed... Going on...")
            contactOf(others.valuesIterator, leadIterator)

        case message@Message(Topics.STOP, _, _) =>
            log.info("I'm out of the game")
            context.become(behavior = OUT, discardOld = true)

        case message@Message(_, _, _: Victory) =>
            context.become(behavior = resistive, discardOld = true)
            OUT(message)

        case _ => parent forward _
    }
    
    val OUT: Receive = {
        
        case message@Message(_, _, newLeader: Victory) =>
            
            parent forward message
            
            this.log.info("The new leader is {} with election ID#{}", newLeader.ref, newLeader.eUID)
            this.actualLeader = newLeader.ref
            this.leaderUID = newLeader.eUID
    
            if (amITheLeader && newLeader.eUID != this.eUID) {
                log.info("I'm no more the leader...")
                this.amITheLeader = false
            }
    
            parent ! Message(content = End())
            
            this.amIInElection = false
            this.leadIterator = expectedLeaders.iterator

        case _ => parent forward _
    }
    
    private def contactOf(all: Iterator[ActorSelection], those: Iterator[ActorSelection]): Unit = {
    
        if (those.hasNext) {
            val next = those.next
            log.info("Contacting {}", next)
            parent ! Message(content = ElectionBully(eUID, self))
            (next ? Message(content = ElectionBully(eUID, self)))
                .onComplete({
                    parent ! Message(Topics.NEXT, Direction.AnyDir, Empty())
                    self ! _.getOrElse(Message(Topics.NEXT, Direction.AnyDir, Empty()))
                })
        } else {
            this becomeLeaderOf all
        }
    }
    
    private def becomeLeaderOf(all: Iterator[ActorSelection]): Unit = {
        this.amITheLeader = true
        log.info("I'm the new Leader!")
        all.foreach(_ ! Message(content = Victory(eUID, self)))
    }
}

object BullyActor {
    
    object Topics {
        val NEXT: Topic = "/NEXT"
        val STOP: Topic = "/STOP"
    }
    
}