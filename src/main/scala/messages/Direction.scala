package messages

/**
  * Created by Alessandro on 04/07/2017.
  */

/**
  * A Message "Header" that specifies the Direction from which the packet is coming
  * and to which the packet is going.
  *
  */
trait Direction {
    
    def from: Location
    
    def to: Location
    
    def reverse: Direction
    
    override def toString: String = "from " + from + " to " + to
    
    override def equals(obj: scala.Any): Boolean = obj match {
        case that: Direction => that.from == this.from && that.to == this.to
        case _ => false
    }
}

final case class DirectionImpl(from: Location, to: Location) extends Direction {
    override def reverse = DirectionImpl(to, from)
}

object Direction {
    
    import Location._
    
    val AnyDir: Direction = "A" >> "B"
    
    implicit def Direction2String(d: Direction): String = String.join(",", d.from, d.to)
}

/**
  * This trait defines a Builder of Direction with the use of the Companion object.
  *
  * Given another Location it creates a Direction from this location to the given location
  *
  */
trait Location {
    
    val loc: String
    
    def >>(that: Location): Direction = DirectionImpl(from = this.loc, to = that.loc)
    
    def <<(that: Location): Direction = DirectionImpl(from = that.loc, to = this.loc)
    
    override def toString: String = loc
    
    override def equals(obj: scala.Any): Boolean = obj match {
        case that: Location => that.loc == this.loc
    }
}

case class LocationImpl(loc: String) extends Location

object Location {
    
    implicit def location2String(d: Location): String = d.toString
    
    implicit def StringToLocation(str: String): Location = LocationImpl(str)
    
}

object TryDirection extends App {
    
    import Location._
    
    "A" >> "B"
}